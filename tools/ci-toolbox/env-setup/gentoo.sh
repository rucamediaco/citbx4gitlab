. $CITBX_ABS_DIR/env-setup/common.sh

INSTALL_PKGS=()

if setup_component_enabled base-pkgs; then
    for pkg in app-emulation/docker sys-apps/gawk dev-python/pyyaml app-misc/jq app-misc/ca-certificates; do
        if ! equery -q list $pkg > /dev/null; then
            INSTALL_PKGS+=($pkg)
        fi
    done
fi
if setup_component_enabled git-lfs; then
    if ! equery -q list dev-vcs/git-lfs > /dev/null; then
        INSTALL_PKGS+=(dev-vcs/git-lfs)
    fi
    INSTALL_PKGS+=(git-lfs)
fi
if [ "${#INSTALL_PKGS[@]}" -gt 0 ]; then
    print_info "Installing packages..."
    _sudo emerge -av "${INSTALL_PKGS[@]}"
fi

if setup_component_enabled base-pkgs; then
    if [ "${USER}" != "root" ]; then
        _sudo gpasswd -a ${USER} docker
    fi
fi

if setup_component_enabled ca-certs; then
    print_info "Installing CA certificates..."
    # Add custom SSL ROOT CAs
    install_ca_certificates_system
    install_ca_certificates_docker
fi

if setup_component_enabled docker-cfg; then
    print_info "Configuring docker..."

    write_daemon_json

    # Put in comment the docker default options
    if grep -q '^DOCKER_OPTS=.*' /etc/conf.d/docker \
        && ! grep -q '^DOCKER_OPTS=""$' /etc/conf.d/docker; then
        _sudo sed -i 's/^DOCKER_OPTS=.*$/DOCKER_OPTS=""/g' /etc/conf.d/docker
    fi
    _sudo ip link del docker0 2>/dev/null || true
    _sudo /etc/init.d/docker restart
fi

if setup_component_enabled ci-tools; then
    print_info "Installing the CI toolbox and wrapper..."
    install_tools ci-tools
fi
