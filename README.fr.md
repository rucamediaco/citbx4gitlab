# citbx4gitlab : Boîte à outils CI pour Gitlab

Cette boîte à outils peut être utilisée pour

* exécuter une tâche Gitlab en dehors d'un exécuteur de tâche Gitlab (outil `gitlab-runner`) sur votre poste de travail (fonction de base)
* écrire la partie script en utilisant une structure modulaire (fonction avancée)

## Table des matières

* [Le projet](#le-projet)
    * [Installation de l'outil ci-toolbox](#installation-de-loutil-ci-toolbox)
    * [Mise à jours de l'outil ci-toolbox](#mise-à-jours-de-loutil-ci-toolbox)
    * [Intégration dans une arborescence projet](#intégration-dans-une-arborescence-projet)
    * [Mise à jour de l'outil ci-toolbox du projet](#mise-à-jour-de-loutil-ci-toolbox-du-projet)
* [Cas d'utilisation : Tâche de pipeline Gitlab standard](#cas-dutilisation--tâche-de-pipeline-gitlab-standard)
* [Exécuter une tâche spécifique localement](#exécuter-une-tâche-spécifique-localement)
    * [Première solution : utiliser l'outil gitlab-runner](#première-solution--utiliser-loutil-gitlab-runner)
    * [La seconde solution : la commande ci-toolbox](#la-seconde-solution--la-commande-ci-toolbox)
* [Écrire une tâche ou un module en utilisant la boîte à outils](#Écrire-une-tâche-ou-un-module-en-utilisant-la-boîte-à-outils)
    * [Écrire une tâche](#Écrire-une-tâche)
    * [Écriture d'un module](#Écriture-dun-module)
    * [Liste des fonctions utiles](#liste-des-fonctions-utiles)
* [Exemples de tâches](#exemples-de-tâches)
    * [job-minimal](#job-minimal)
    * [job-with-before-after-script](#job-with-before-after-script)
    * [job-advanced](#job-advanced)
    * [job-test-services-mysql](#job-test-services-mysql)
    * [job-test-services-postgres](#job-test-services-postgres)

## Le projet

L'outil ci-toolbox peut être installé dans le système et/ou intégré dans l'arborescence de votre projet (ce qui est utile pour forcer l'équipe du projet à utiliser une version spécifique et ainsi maîtriser les mises à jours)

La commande ci-toolbox (`/usr/local/bin/ci-toolbox`) est un outil intermédiaire qui va appeler `tools/ci-toolbox/ci-toolbox.sh` depuis le projet si il est trouvé dedans, autrement, il va exécuter celui installé dans le système (`/usr/local/lib/ci-toolbox/ci-toolbox.sh`)

### Installation de l'outil ci-toolbox

L'installation peut se faire :
* Manuellement en clonant le projet et en lançant le script `./tools/ci-toolbox/ci-toolbox.sh setup`
* En copiant ce qui suit dans une invite de commande (en remplaçant si besoin 'master' par le numéro de version choisi dans l'URL) :

```bash
mkdir -p /tmp/ci-toolbox \
    && curl -fSsL https://gitlab.com/ercom/citbx4gitlab/repository/master/archive.tar.bz2 \
    | tar -xj -C /tmp/ci-toolbox \
    && cd /tmp/ci-toolbox/* \
    && ./tools/ci-toolbox/ci-toolbox.sh setup \
    && cd - \
    && rm -rf /tmp/ci-toolbox
```

### Mise à jours de l'outil ci-toolbox

Il suffit de taper la commande `ci-toolbox setup --component ci-tools` ou `ci-toolbox setup --component ci-tools <custom_version>`

### Intégration dans une arborescence projet

Cas d'intégration de l'outil ci-toolbox dans une arborescence projet :
```
├── .gitlab-ci.yml
├── tools
│   └── ci-toolbox
│       ├── 3rdparty
│       │   └── bashopts.sh
│       ├── env-setup
│       │   ├── common.sh
│       │   ├── gentoo.sh
│       │   └── ubuntu.sh
│       ├── modules
│       │   ├── ccache.sh
│       │   └── example.sh
│       ├── jobs
│       │   └── job-advanced.sh
│       ├── ci-toolbox.properties
│       └── ci-toolbox.sh
```

Liste des éléments essentiels :

* `.gitlab-ci.yml` : Définition du pipeline de tâches Gitlab
* `ci-toolbox.sh` : script de lancement de tâche Gitlab-CI exécuté indirectement via la commande `ci-toolbox`
* `bashopts.sh` : dépendance externe nécessaire pour la boîte à outils

Liste des éléments recommandés :

* `env-setup` : dossier contenant les routines d'installation de l'environnement pour faire tourner la boîte à outils
* `ci-toolbox.properties` : Propriétés de la boîte à outils propre au projet cible

Liste des éléments pour une utilisation avancée :

* `jobs` : dossier contenant les fonctions spécifiques aux différentes tâches
* `modules` : dossier contenant les modules

### Mise à jour de l'outil ci-toolbox du projet

La commande `ci-toolbox update` ou `ci-toolbox update <custom_version>` permet de mettre à jour de l'outil ci-toolbox de votre projet

## Cas d'utilisation : Tâche de pipeline Gitlab standard

Le schéma suivant décrit l'exécution d'une tâche standard appelée "J" dans un pipeline Gitlab-CI

![Cas d'utilisation global](doc/GitlabCIPipelineJob.png)

Dans ce cas, il serait intéressant d'exécuter une tâche spécifique comme J dans leur environnement approprié sur votre poste de travail local sans avoir à valider un enregistrement (`git commit`) ni avoir à pousser votre dépôt (`git push`).

Le but est d'avoir exactement le même environnement de construction sur l'exécuteur de tâche Gitlab et votre poste de travail local.

![Cas d'utilisation d'une tâche unique](doc/GitlabCISingleJob.png)

## Exécuter une tâche spécifique localement

### Première solution : utiliser l'outil gitlab-runner

Pour utiliser cette solution, vous devrez :

* installer l'outil `gitlab-runner` sur le poste de travail
* et le lancer de la manière suivante : `gitlab-runner exec <type d'exécuteur> <nom de la tâche>`

Vous devrez en plus :

* valider tous les changements locaux dans des enregistrement GIT
* ajouter des options supplémentaires à `gitlab-runner` du type `--docker-image` avec le nom de l'image appropriée

**NOTE :** La commande `gitlab-runner exe` dépréciée depuis Gitlab 10, et donc susceptible de disparaître

### La seconde solution : la commande ci-toolbox

Cette boîte à outils est capable de le faire :

* démarrer une tâche  de type script ou `docker` avec les paramètres appropriés (image, etc.)
* lancer une tâche du pipeline localement sans avoir à valider un enregistrement (`git commit`)

Fonctions additionnelles pour une utilisation avancée :

* Ajouter des paramètres et des actions spécifiques au poste de travail
* Ajouter la possibilité d'exécuter une invite de commande dans l'environnement docker approprié
* Diviser les différentes tâche du pipeline de façon modulaire

Utilisation :

* `ci-toolbox <commande> [arguments...]` depuis n'importe quel endroit dans l'arborescence du projet, ... ou bien :
* `chemin/du/ci-toolbox.sh <commande> [arguments...]`

L'outil `ci-toolbox`  est installé dans le système durant la phase de setup `path/to/ci-toolbox.sh setup`, et ajoute les fonctionnalités suivantes :

* Cet outil peut être exécuté n'importe où dans l'arborescence du projet contenant le script `ci-toolbox.sh`
* Cet outil est comme un raccourci pour trouver et exécuter le script `ci-toolbox.sh` sans avoir à spécifier le chemin absolu/relatif
* Cet outil permet d'ajouter la prise en charge de l'auto complétion BASH sur le nom des tâches du pipeline Gitlab et des options associées.

Les limites connues de la boîte à outils :

* Applicable uniquement pour les types d'exécuteur de tâche Gitlab `docker` et `shell`

## Écrire une tâche ou un module en utilisant la boîte à outils

En plus de pouvoir lancer une tâche du pipeline, (comme la tâche [job-minimal](#job-minimal)), vous pouvez utiliser la boîte à outils pour écrire les scripts de celle-ci avec une structure modulaire comme c'est le cas pour la tâche d'exemple [job-advanced](#job-advanced)

![Ordre d'exécution des type de routines](doc/HookExecutionOrder.png)

### Écrire une tâche

Les routines spécifiques à la tâche en question doivent être écrites dans un script qui porte le nom de cette dernière dans le dossier : `jobs/<nom de la tâche>.sh`

Cette tâche peut faire appel à différents modules utilisant : `citbx_use "<nom du module>"`

La tâche peut définir les routines suivantes - Uniquement applicables à un poste de travail local :

* `job_define()` : Cette fonction peut être utilisée pour définir des options pour le cas d'utilisation sur un poste de travail
* `job_setup()` : Cette fonction peut être utilisée pour effectuer une action avant de configurer et démarrer l'environnement de la tâche (docker run)

La tâche peut définir les routines suivantes - applicables à tous les environnements :

* `job_main()` : La fonction principale de la tâche qui contient la charge utile
* `job_after()` : Cette fonction peut être utilisée pour effectuer une action après l'exécution de la fonction `job_main`. Cette fonction est appelée dans TOUS les cas (succès ou erreur). Cette fonction est appelée avec le code de retour du processus comme premier argument (sera égal à 0 sur succès)

Exemple de script de tâche : [tools/ci-toolbox/jobs/job-advanced.sh](tools/ci-toolbox/jobs/job-advanced.sh)

### Écriture d'un module

Le module doit être défini dans `modules/<nom de mon module>.sh`.

Ce module peut utiliser d'autres modules à l'aide de `citbx_use'"<mon autre module>"`.

Le module peut définir les routines suivantes - Uniquement applicables à un poste de travail local :

* `citbx_module_<nom de mon module>_define()` : Cette fonction peut être utilisée pour définir des options pour le cas d'utilisation sur un poste de travail
* `citbx_module_<nom de mon module>_setup()` : Cette fonction peut être utilisée pour effectuer une action avant de configurer et démarrer l'environnement de la tâche (`docker run`)

La tâche peut définir les routines suivantes - applicables à tous les environnements :

* `citbx_module_<nom de mon module>_before()` : Cette fonction peut être utilisée pour réaliser des actions avant l'exécution de la fonction principale de la tâche `job_main`
* `citbx_module_<nom de mon module>_after()` : Cette fonction peut être utilisée pour réaliser des actions après l'exécution de la fonction principale de la tâche `job_main`. Cette fonction est appelée dans tous les cas (succès ou erreur) avec comme premier argument le code de retour du dernier processus exécuté dans la fonction `job_main` (sera égal à 0 en cas de succès)

Exemple de script de module : [tools/ci-toolbox/modules/example.sh](tools/ci-toolbox/modules/example.sh)

### Liste des fonctions utiles

* `citbx_run_ext_job <nom de la tâche>` : Exécuter une autre tâche
* `citbx_job_list [prefix]` : Obtenir la liste des tâches (en option : avec le préfixe spécifié)
* `citbx_use <nom du module>` : Charger un module
* `print_critical <message>` : Affichage d'un message d'erreur et sortie (code de retour : 1)
* `print_error <message>` : Affichage un message d'erreur
* `print_warning <message>` : Affichage un message d'avertissement
* `print_note <message>` : Affichage un message de note
* `print_info <message>` : Affichage un message d'information

## Exemples de tâches

### job-minimal

Exemple de de tâche docker minimaliste

Définition de la tâche :

```yaml
image: ubuntu:16.04

job-minimal:
  stage: all-in-one
  script:
    - echo "Bonjour monde !"

after_script:
  - echo "job ${CI_JOB_NAME} end"
```

Exécution de la tâche :

![Tâche minimaliste](doc/TestCaseJobMinimal.png)

### job-with-before-after-script

Exemple de définition de tâche docker avec les propriétés `before_script` et `after_script`

Définition de la tâche :

```yaml
after_script:
  - echo "job ${CI_JOB_NAME} end"

job-with-before-after-script:
  stage: all-in-one
  before_script:
    - echo "exécuté avant"
  script:
    - echo "script"
      "sur plusieurs"
    - echo "lignes"
    - cat <<< $(echo 'salut !')
  after_script:
    - echo "exécuté après"
```

### job-advanced

Exemple de définition d'une tâche docker (fichier [tools/ci-toolbox/jobs/job-advanced.sh](tools/ci-toolbox/jobs/job-advanced.sh)) avec options, arguments, traitements supplémentaires et utilisation de modules externes (fichier [tools/ci-toolbox/modules/example.sh](tools/ci-toolbox/modules/example.sh))

Définition de la tâche :

```yaml
job-advanced:
  image: ubuntu:16.04
  stage: all-in-one
  variables:
    JOBADVAR: "${CI_JOB_NAME} JOBADVAR value"
  script:
    - echo ${GLOBALJOBVAR}
    - echo ${JOBADVAR}
    - tools/ci-toolbox/ci-toolbox.sh

after_script:
  - echo "job ${CI_JOB_NAME} end"
```

Exécution de la tâche :

![Tâche avancée](doc/TestCaseJobAvanced.png)

### job-test-services-mysql

Exemple de définition de tâche docker avec un service MySQL

Définition de la tâche :

```yaml
job-test-services-mysql:
  stage: build
  variables:
    MYSQL_DATABASE: test
    MYSQL_ROOT_PASSWORD: password
  image: mysql
  services:
    - mysql
  tags:
    - docker
  script:
    - printf "Waiting for mysql";
      while ! mysql -h mysql -u root -ppassword test -e '' > /dev/null 2>&1 ;
      do printf "."; sleep 1; done;
      printf " done!\n"
    - mysql -h mysql -u root -ppassword test -e 'SHOW VARIABLES LIKE "%version%";'
```

### job-test-services-postgres

Exemple de définition de tâche docker avec un service PostgreSQL

Définition de la tâche :

```yaml
job-test-services-postgres:
  stage: build
  image: postgres:9.4
  services:
    - name: postgres:9.4
      alias: db-postgres
      entrypoint: ["docker-entrypoint.sh"]
      command: ["postgres"]
  tags:
    - docker
  script:
    - printf "Waiting for postgres";
      while ! psql -h db-postgres -U postgres -c '' > /dev/null 2>&1 ;
      do printf "."; sleep 1; done;
      printf " done!\n"
    - psql -h db-postgres -U postgres -c 'select version();'
```
