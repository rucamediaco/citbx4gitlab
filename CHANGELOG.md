# Cangelog

## 4.1.0
* [env-setup/common] update install_tools function
* [setup/upgrade] Add a migration tool from version 2 and 3 to 4 and upper
* [ci-toolbox] Add OS_RELEASE_INFO hashmap
* [ci-toolbox] Fix in docker script part to be more portable on distribution like Fedora
* [wrapper] Move wrapper directory outside of tools/ci-toolbox
* [ci-toolbox] Increase portability by using /usr/bin/env in shebang
* [README] README files update

## 4.0.0
* [README] Add French version
* [LICENSE] Remove instruction section at the end
* [CI] Add docker tag
* [ci-toolbox/setup] Add --os-id option
* [ci-toolbox/git-lfs] Remove CITBX_GIT_LFS_ENABLED option and git lfs pull no more useful
* [ci-toolbox] Add support of ci-toolbox.sh stored outside project repo
    + Rename script run.sh to ci-toolbox.sh
    + Rename directories run.d => jobs and gitlab-ci => ci-toolbox
    + Run jobs from project containing .gitlab-ci.yml without having to integrate ci-toolbox into this one
    + Update ci-toolbox setup an update process
* [ci-toolbox] Fix colors display with some shells like dash
* [ci-toolbox] Remove CITBX_WAIT_FOR_SERVICE_START option
* [ci-toolbox] Add docker-prune command
* [ci-toolbox] set CITBX_DOCKER_LOGIN_MODE default value to disabled
* [ci-toolbox] Set non interactive mode as default mode
* [ci-toolbox] Rewrite CITBX_COMMANDS and CITBX_DOCKER_SCRIPT for better readability and compatibility with gitlab-runner operating

## 3.3.1
* [executor/docker] Add support to busybox based image and use addgroup instead of usermod
* [FIX] broken --docker-login=enabled & improve contextual information message
* [module/dockerimg] file moved to ercom/docker project
* [CA] Fill CI_SERVER_TLS_CA_FILE with local CA certs on local worstation

## 3.3.0
* [env-setup] Improve ca-certificates setup part
* [modules/dockerimg] Update dockerimg module
* [modules/example] minor fix on variable export
* Remove ci-job-wrapper jobs and update the documentation
* Update docker-login option

## 3.2.0
* [env-setup] Modular setup
  + add ability to limit setup on specified elements
  + add ci-tool setup
  + add gentoo support

## 3.1.0
* [FIX/bashcomp] Fix number calculation in the _citbx4gitlab_compgen function
* Add disable-service parameter (CITBX_DISABLED_SERVICES) to disable specified services

## 3.0.1
* [FIX] regression: CITBX_TOOL_NAME not defined when tools/gitlab-ci/run.sh is called directly

## 3.0.0
* Add support of bash completion
* [FIX] remove warning from a test
* [FIX] unset CITBX_GIT_CLEAN for citbx_run_ext_job
* [CI] add job-test-services-mysql and job-test-services-postgres service example jobs
* Update docker run to use CITBX_JOB_SHELL
* [FIX] YAML variable treatment
* Improve messages/help formating
* Add debug-script option
* Remove deprecated JOB_EXT_FILE_NAME variable name
* [3rd] Update bashopts to the version 2.0.0
* Update fetch_file function for update command
* [FIX] docker run exit code on error
* [FIX] Preserve PATH environment variable in user mode
* Add VERSION and a CHANGELOG.md

## 2.2.10
* [FIX] unset CITBX_JOB_RUN_FILE_NAME in citbx_run_ext_job

## 2.2.9
* Improve git worktree support and preserve the working directory on shell mode

## 2.2.8
* Update variable name: CITBX_JOB_RUN_FILE_NAME

## 2.2.7
* Add ncore function

## 2.2.6
* [env-setup/ubuntu] Fix GIT LFS repository

## 2.2.5
* [env-setup/ubuntu] Update GIT LFS apt repository add

## 2.2.4
* Set git lfs as an optional feature

## 2.2.3
* [run] improve docker-dns detection

## 2.2.2
* [env-setup/ubuntu] [Fix] ca-certificates copy
* [Fix] read empty or invalid daemon.json

## 2.2.1
* [env-setup/ubuntu] Add --allow-change-held-packages option to apt-get remove docker.io & docker-engine
* [run] improve CI PROJECT ROOT DIR detection

## 2.2
* [FIX] service-privileged property variable name update
* Add GIT LFS support
* Improve docker-dns option check

## 2.1.1
* [FIX] service with custom commands with default entrypoint
* Add option to start service in privileged mode

## 2.1
* Add git-clean and group options

## 2.0.2
* [FIX] Eval YAML service properties

## 2.0.1
* [FIX] export variables to services

## 2.0
* Add Gitlab services support

## 1.3.2
* [fix] Prevent multiple call of citbx_job_finish

## 1.3.1
* add SIGINT SIGTERM to citbx_job_finish trap callback

## 1.3.0
* [fix] return the good error code on local job execution failure
* Add citbx_docker_run_add_args function

## 1.2.1
* [3rd] Update bashopts to the version 1.3.0
* Add citbx_export function
* [fix] Variable export the SHELL executor

## 1.2
* Changes:
* Add a check on the bash version
* Add submodule-strategy option for GIT_SUBMODULE_STRATEGY
* Add job executor option
* Add job script and job main func check
* Add image entrypoint support
* Remove CITBX_DOCKER_USER option
* reset PWD to CI_PROJECT_DIR before hooks execution
* [FIX] ubuntu setup
* Update the documentation

## 1.1
* Add CITBX_DEFAULT_JOB_SHELL property and CITBX_JOB_SHELL option
* Add job execution time
* Add update tool to fetch the last version
* Add gitlab GIT_SUBMODULE_STRATEGY support

## 1.0
* First stable release
