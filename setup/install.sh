
if [ ! -v CITBX_ABS_DIR ] \
    || [ ! -v CITBX_SETUP_TYPE ] \
    || [ ! -v BASHOPTS_FILE_PATH ] \
    || [ ! -v CITBX_SRC_DIR ]; then
    printf "\033[91m[!!] This file cannot be executed outside the suitable environment!\033[0m\n"
    printf "\033[92m[II] Please use instead the following command: tools/ci-toolbox/ci-toolbox.sh setup\033[0m\n"
    exit 1
fi

case "$CITBX_SETUP_TYPE" in
    project)
        if [ "$CITBX_ABS_DIR" != "/usr/local/lib/ci-toolbox" ]; then
            cp -v $CITBX_SRC_DIR/tools/ci-toolbox/ci-toolbox.sh $CITBX_ABS_DIR/ci-toolbox.sh
            chmod +x $CITBX_ABS_DIR/ci-toolbox.sh
            mkdir -p $CITBX_ABS_DIR/env-setup
            cp -v $CITBX_SRC_DIR/tools/ci-toolbox/env-setup/* $CITBX_ABS_DIR/env-setup/
            cp -v $CITBX_SRC_DIR/tools/ci-toolbox/3rdparty/bashopts.sh $BASHOPTS_FILE_PATH
            if [ -f $CITBX_ABS_DIR/ci-toolbox.properties ]; then
                cp -v $CITBX_SRC_DIR/tools/ci-toolbox/ci-toolbox.properties $CITBX_ABS_DIR/ci-toolbox.properties.default
            else
                cp -v $CITBX_SRC_DIR/tools/ci-toolbox/ci-toolbox.properties $CITBX_ABS_DIR/ci-toolbox.properties
            fi
        fi
    ;;
    ci-tools)
        _sudo mkdir -p /usr/local/lib/ci-toolbox/{env-setup,3rdparty}
        _sudo cp -v $CITBX_SRC_DIR/tools/ci-toolbox/ci-toolbox.sh /usr/local/lib/ci-toolbox/ci-toolbox.sh
        _sudo chmod +x /usr/local/lib/ci-toolbox/ci-toolbox.sh
        _sudo cp -v $CITBX_SRC_DIR/tools/ci-toolbox/env-setup/* /usr/local/lib/ci-toolbox/env-setup/
        _sudo cp -v $CITBX_SRC_DIR/tools/ci-toolbox/3rdparty/bashopts.sh /usr/local/lib/ci-toolbox/3rdparty/
        _sudo cp -v $CITBX_SRC_DIR/wrapper/ci-toolbox /usr/local/bin/
        _sudo mkdir -p /etc/bash_completion.d
        _sudo cp -v $CITBX_SRC_DIR/wrapper/bashcomp /etc/bash_completion.d/ci-toolbox
        if [ -f /usr/local/lib/ci-toolbox/ci-toolbox.properties ]; then
            _sudo cp -v $CITBX_SRC_DIR/tools/ci-toolbox/ci-toolbox.properties /usr/local/lib/ci-toolbox/ci-toolbox.properties.default
        else
            _sudo cp -v $CITBX_SRC_DIR/tools/ci-toolbox/ci-toolbox.properties /usr/local/lib/ci-toolbox/ci-toolbox.properties
        fi
    ;;
    *)
        print_critical "Invalid installation type '$CITBX_SETUP_TYPE'"
    ;;
esac
